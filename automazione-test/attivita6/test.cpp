#define CATCH_CONFIG_MAIN

#include "../catch.hpp"

#include "operatori.h"

TEST_CASE("operatori") {
  int iniziale = GENERATE(take(1000, random(-1000000000, 1000000000)));
  int a;
  // arrange
  a = iniziale;
  SECTION("incremento") {
    // arrange
    a = iniziale;
    SECTION("incremento") {
      // act
      inc(&a);
      // asser)t
      REQUIRE(a == iniziale + 1);
      SECTION("incremento") {
        inc(&a);
        REQUIRE(a == iniziale + 2);
      }
      SECTION("decremento") {
        dec(&a);
        REQUIRE(a == iniziale);
      }
    }
    SECTION("decremento") {
      // act5
      dec(&a);
      // assert
      REQUIRE(a == iniziale - 1);
      SECTION("incremento") {
        inc(&a);
        REQUIRE(a == iniziale);
      }
      SECTION("decremento") {
        dec(&a);
        REQUIRE(a == iniziale - 2);
      }
    }
  }
}