#define CATCH_CONFIG_MAIN

#include "../catch.hpp"

#include "operatori.h"

TEST_CASE("operatori") {
  int a;
  // arrange
  a = 5;
  SECTION("incremento") {
    // act
    inc(&a);
    // asser)t
    REQUIRE(a == 6);
    SECTION("incremento"){
        inc(&a);
        REQUIRE(a==7);
    }
    SECTION("decremento"){
        dec(&a);
        REQUIRE(a==5);
    }
  }
  SECTION("decremento"){
    // act
    dec(&a);
    // assert
    REQUIRE(a == 4);
    SECTION("incremento"){
        inc(&a);
        REQUIRE(a==5);
    }
    SECTION("decremento"){
        dec(&a);
        REQUIRE(a==3);
    }
  }
}