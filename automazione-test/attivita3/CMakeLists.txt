cmake_minimum_required(VERSION 3.15)
project(fattoriale CXX)

add_library(fattoriale src/fattoriale.cpp)
target_include_directories(fattoriale PUBLIC include)

set_target_properties(fattoriale PROPERTIES PUBLIC_HEADER "include/fattoriale.h")
install(TARGETS fattoriale)
