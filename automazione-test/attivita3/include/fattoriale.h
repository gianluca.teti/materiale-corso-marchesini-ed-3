#pragma once

#ifdef _WIN32
  #define fattoriale_EXPORT __declspec(dllexport)
#else
  #define fattoriale_EXPORT
#endif

fattoriale_EXPORT int fatt(int n);
