#define CATCH_CONFIG_MAIN

#include "../catch.hpp"
#include "fattoriale.h"

TEST_CASE("funzione fattoriale"){
    REQUIRE(fatt(1) == 1);
    REQUIRE(fatt(5) == 10);
}