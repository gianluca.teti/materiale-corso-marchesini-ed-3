#define CATCH_CONFIG_MAIN

#include "../catch.hpp"

#include "aritmetica.h"

TEST_CASE("elemento neutro") {
  int x = GENERATE(range(-100, 100));
  REQUIRE(add(x, 0) == x);
}

TEST_CASE("associatività") {
  int x = GENERATE(range(-100, 100));
  int y = GENERATE(range(-100, 100));
  int z = GENERATE(range(-100, 100));
  REQUIRE(add(add(x, y), z) == add(x, add(y, z)));
}

TEST_CASE("commutatività"){
  int x = GENERATE(range(-100,100));
  int y = GENERATE(range(-100,100));
  REQUIRE(add(x,y) == add(y,x));
}